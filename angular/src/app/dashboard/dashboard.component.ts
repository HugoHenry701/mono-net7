import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  template: `
    <app-host-dashboard *abpPermission="'NuNuMono.Dashboard.Host'"></app-host-dashboard>
    <app-tenant-dashboard *abpPermission="'NuNuMono.Dashboard.Tenant'"></app-tenant-dashboard>
  `,
})
export class DashboardComponent {}
