import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

const oAuthConfig = {
  issuer: 'https://localhost:44374/',
  redirectUri: baseUrl,
  clientId: 'NuNuMono_App',
  responseType: 'code',
  scope: 'offline_access NuNuMono',
  requireHttps: true,
};

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'NuNuMono',
  },
  oAuthConfig,
  apis: {
    default: {
      url: 'https://localhost:44387',
      rootNamespace: 'NuNuMono',
    },
    AbpAccountPublic: {
      url: oAuthConfig.issuer,
      rootNamespace: 'AbpAccountPublic',
    },
  },
} as Environment;
