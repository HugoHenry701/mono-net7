﻿using NuNuMono.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace NuNuMono.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class NuNuMonoController : AbpControllerBase
{
    protected NuNuMonoController()
    {
        LocalizationResource = typeof(NuNuMonoResource);
    }
}
