using NuNuMono.MongoDB;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace NuNuMono.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(NuNuMonoMongoDbModule),
    typeof(NuNuMonoApplicationContractsModule)
)]
public class NuNuMonoDbMigratorModule : AbpModule
{

}
