using NuNuMono.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace NuNuMono.Permissions;

public class NuNuMonoPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(NuNuMonoPermissions.GroupName, L("Permission:BookStore"));

        myGroup.AddPermission(NuNuMonoPermissions.Dashboard.Host, L("Permission:Dashboard"), MultiTenancySides.Host);
        myGroup.AddPermission(NuNuMonoPermissions.Dashboard.Tenant, L("Permission:Dashboard"), MultiTenancySides.Tenant);

        //Define your own permissions here. Example:
        //myGroup.AddPermission(NuNuMonoPermissions.MyPermission1, L("Permission:MyPermission1"));
        var booksPermission = myGroup.AddPermission(NuNuMonoPermissions.Books.Default, L("Permission:Books"));
        booksPermission.AddChild(NuNuMonoPermissions.Books.Create, L("Permission:Books.Create"));
        booksPermission.AddChild(NuNuMonoPermissions.Books.Edit, L("Permission:Books.Edit"));
        booksPermission.AddChild(NuNuMonoPermissions.Books.Delete, L("Permission:Books.Delete"));
        var authorsPermission = myGroup.AddPermission(NuNuMonoPermissions.Authors.Default, L("Permission:Authors"));
        authorsPermission.AddChild(NuNuMonoPermissions.Authors.Create, L("Permission:Authors.Create"));
        authorsPermission.AddChild(NuNuMonoPermissions.Authors.Edit, L("Permission:Authors.Edit"));
        authorsPermission.AddChild(NuNuMonoPermissions.Authors.Delete, L("Permission:Authors.Delete"));

    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<NuNuMonoResource>(name);
    }
}
