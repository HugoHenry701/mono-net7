﻿using System;
using Volo.Abp.Application.Dtos;
namespace NuNuMono.Authors
{
    public class GetAuthorListDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}

