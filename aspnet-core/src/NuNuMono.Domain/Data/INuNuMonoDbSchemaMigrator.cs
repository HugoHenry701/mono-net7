﻿using System.Threading.Tasks;

namespace NuNuMono.Data;

public interface INuNuMonoDbSchemaMigrator
{
    Task MigrateAsync();
}
