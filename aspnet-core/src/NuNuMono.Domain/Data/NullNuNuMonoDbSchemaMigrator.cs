﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace NuNuMono.Data;

/* This is used if database provider does't define
 * INuNuMonoDbSchemaMigrator implementation.
 */
public class NullNuNuMonoDbSchemaMigrator : INuNuMonoDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
