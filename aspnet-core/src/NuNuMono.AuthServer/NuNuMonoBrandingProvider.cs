﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace NuNuMono;

[Dependency(ReplaceServices = true)]
public class NuNuMonoBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "NuNuMono";
}
