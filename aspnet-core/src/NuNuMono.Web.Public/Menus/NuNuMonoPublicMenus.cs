﻿namespace NuNuMono.Web.Public.Menus;

public class NuNuMonoPublicMenus
{
    private const string Prefix = "NuNuMono.Public";

    public const string HomePage = Prefix + ".HomePage";

    public const string ArticleSample = Prefix + ".SampleArticle";

    public const string ContactUs = Prefix + ".ContactUs";
}
