﻿using NuNuMono.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace NuNuMono.Web.Public.Pages;

/* Inherit your Page Model classes from this class.
 */
public abstract class NuNuMonoPublicPageModel : AbpPageModel
{
    protected NuNuMonoPublicPageModel()
    {
        LocalizationResourceType = typeof(NuNuMonoResource);
    }
}
