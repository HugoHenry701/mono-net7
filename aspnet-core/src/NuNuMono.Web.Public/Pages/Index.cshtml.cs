﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;

namespace NuNuMono.Web.Public.Pages;

public class IndexModel : NuNuMonoPublicPageModel
{
    public void OnGet()
    {

    }

    public async Task OnPostLoginAsync()
    {
        await HttpContext.ChallengeAsync("oidc");
    }
}
