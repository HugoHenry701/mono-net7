﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using MongoDB.Driver.Linq;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;
using NuNuMono.MongoDB;

namespace NuNuMono.Authors
{
    public class MongoDbAuthorRepository
    : MongoDbRepository<NuNuMonoMongoDbContext, Author, Guid>,
    IAuthorRepository
    {
        public MongoDbAuthorRepository(
            IMongoDbContextProvider<NuNuMonoMongoDbContext> dbContextProvider
            ) : base(dbContextProvider)
        {
        }

        public async Task<Author> FindByNameAsync(string name)
        {
            var queryable = await GetMongoQueryableAsync();
            return await queryable
                .FirstOrDefaultAsync(author => author.Name == name);
        }

        public async Task<List<Author>> GetListAsync(
            int skipCount,
            int maxResultCount,
            string sorting,
            string filter = null)
        {
            var queryable = await GetMongoQueryableAsync();
            return await queryable
                .WhereIf<Author, IMongoQueryable<Author>>(
                    !filter.IsNullOrWhiteSpace(),
                    author => author.Name.Contains(filter)
                )
                .OrderBy(sorting)
                .As<IMongoQueryable<Author>>()
                .Skip(skipCount)
                .Take(maxResultCount)
                .ToListAsync();
        }
    }
}

