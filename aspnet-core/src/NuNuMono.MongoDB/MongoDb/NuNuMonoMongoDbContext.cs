﻿using MongoDB.Driver;
using NuNuMono.Authors;
using NuNuMono.Books;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace NuNuMono.MongoDB;

[ConnectionStringName("Default")]
public class NuNuMonoMongoDbContext : AbpMongoDbContext
{

    /* Add mongo collections here. Example:
     * public IMongoCollection<Question> Questions => Collection<Question>();
     */
    public IMongoCollection<Book> Books => Collection<Book>();
    public IMongoCollection<Author> Authors => Collection<Author>();
    //...

    protected override void CreateModel(IMongoModelBuilder modelBuilder)
    {
        base.CreateModel(modelBuilder);

        //builder.Entity<YourEntity>(b =>
        //{
        //    //...
        //});
    }
}
