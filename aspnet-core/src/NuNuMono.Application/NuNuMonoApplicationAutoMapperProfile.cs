using AutoMapper;
using NuNuMono.Authors;
using NuNuMono.Books;

namespace NuNuMono;

public class NuNuMonoApplicationAutoMapperProfile : Profile
{
    public NuNuMonoApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
        CreateMap<Book, BookDto>();
        CreateMap<Author, AuthorDto>();
        CreateMap<Author, AuthorLookupDto>();
        CreateMap<CreateUpdateBookDto, Book>();
    }
}
