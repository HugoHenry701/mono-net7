﻿using NuNuMono.Localization;
using Volo.Abp.Application.Services;

namespace NuNuMono;

/* Inherit your application services from this class.
 */
public abstract class NuNuMonoAppService : ApplicationService
{
    protected NuNuMonoAppService()
    {
        LocalizationResource = typeof(NuNuMonoResource);
    }
}
