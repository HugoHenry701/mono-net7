using NuNuMono.MongoDB;
using Volo.Abp.Modularity;

namespace NuNuMono;

[DependsOn(
    typeof(NuNuMonoMongoDbTestModule)
    )]
public class NuNuMonoDomainTestModule : AbpModule
{

}
