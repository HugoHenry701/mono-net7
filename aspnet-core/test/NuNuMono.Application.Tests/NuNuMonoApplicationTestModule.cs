﻿using Volo.Abp.Modularity;

namespace NuNuMono;

[DependsOn(
    typeof(NuNuMonoApplicationModule),
    typeof(NuNuMonoDomainTestModule)
    )]
public class NuNuMonoApplicationTestModule : AbpModule
{

}
